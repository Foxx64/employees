package salaryСalculation;

import java.util.ArrayList;


public class Employee implements  Comparable <Employee>{
	private String name;
	private String role;	
	private int fixedSalary;
	private double experience;
	private ArrayList<Project> projects = new ArrayList<>();
	
	public void addProject (Project project){
		projects.add(project);
	}

	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public int getFixedSalary() {
		return fixedSalary;
	}
	
	public void setFixedSalary(int fixedSalary) {
		this.fixedSalary = fixedSalary;
	}
	
	public double getExperienсe() {
		return experience;
	}
	
	public void setExperience(double experience) {
		this.experience = experience;
	}
	
	public double getBonuses() {
		int fixedCostCount = 0;
		for (int i = 0; projects.size() > i; i++){
			if (!projects.get(i).getFixedCost()){
				fixedCostCount++;
			}
		}
		double timeBonuses = 0;
		if (getExperienсe() > 1){
			timeBonuses = 50 * (getExperienсe() - 1);
		} 
		final double PROJECT_BONUSES = 500;
		double bonuses = timeBonuses + (PROJECT_BONUSES * fixedCostCount);
		return bonuses;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getProjectsCount (){
		int projectSize = projects.size();
		return projectSize;
	}
	
	public Project getProject(int projectNumber){
		Project projectName = projects.get(projectNumber);
		return projectName;
	}
	
	public double getFullSalary() {
		double fullSalary = this.fixedSalary + this.getBonuses();
		return fullSalary;
	}
	
	@Override
	public int compareTo(Employee arg0) {
		return getFullSalary() < arg0.getFullSalary() ? -1 :
			(getFullSalary() == arg0.getFullSalary() ? 0:1);
	} 
	
}
