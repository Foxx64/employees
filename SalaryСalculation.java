package salaryСalculation;

import java.util.ArrayList;
import java.util.Collections;

public class SalaryСalculation {
	ArrayList<Employee> employees = new ArrayList<>();
	SalaryСalculation() {
		Project chartProject = createCharEditorProject();
		Employee ivanEmployee = createIvanEmployee();
		ivanEmployee.addProject(chartProject);
		employees.add(ivanEmployee);
		
		Project employmentSystemProject = createEmploymentSystemProject();
		Employee petrEpmploye = createPetrovEmployee();
		petrEpmploye.addProject(employmentSystemProject);
		employees.add(petrEpmploye);
		
		Project mikeThirdProject = createBankTranslationProject();
		Employee mikeEpmploye = createMikeEmployee();
		mikeEpmploye.addProject(mikeThirdProject);
		mikeEpmploye.addProject (chartProject);
		mikeEpmploye.addProject(employmentSystemProject);
		employees.add(mikeEpmploye);
		
		Employee jimEmployee = createJimEmployee();
		jimEmployee.addProject(chartProject);
		employees.add(jimEmployee);
		print();
		printAll();
	}

	private Employee createJimEmployee() {
		Employee jimEmployee= new Employee();
		jimEmployee.setName("Jim");
		jimEmployee.setRole("developer");
		jimEmployee.setFixedSalary(3500);
		jimEmployee.setExperience(2);
		return jimEmployee;
	}

	private Employee createMikeEmployee() {
		Employee mikeEpmploye = new Employee();
		mikeEpmploye.setName("Mike");
		mikeEpmploye.setRole("Manager");
		mikeEpmploye.setFixedSalary(6000);
		mikeEpmploye.setExperience(4);
		return mikeEpmploye;
	}

	private Project createBankTranslationProject() {
		Project mikeThirdProject = new Project();
		mikeThirdProject.setName("Bank Transaction");
		mikeThirdProject.setFixedCost(false);
		return mikeThirdProject;
	}

	private Employee createPetrovEmployee() {
		Employee petrEpmploye = new Employee();
		petrEpmploye.setName("Petrov Petr");
		petrEpmploye.setRole("developer");
		petrEpmploye.setFixedSalary(2000);
		petrEpmploye.setExperience(2);
		return petrEpmploye;
	}

	private Project createEmploymentSystemProject() {
		Project employmentSystemProject= new Project();
		employmentSystemProject.setName("Employment System");
		employmentSystemProject.setFixedCost(false);
		return employmentSystemProject;
	}

	private Employee createIvanEmployee() {
		Employee ivanEmployee = new Employee();
		ivanEmployee.setName("Ivanov Ivan");
		ivanEmployee.setRole("developer");
		ivanEmployee.setFixedSalary(1700);
		ivanEmployee.setExperience(0.5);
		return ivanEmployee;
	}

	private Project createCharEditorProject() {
		Project chartProject = new Project();
		chartProject.setName("Chart Editor ");
		chartProject.setFixedCost(true);
		return chartProject;
	} 
	
	private void printAll(){

		for (int i = 0; i < employees.size(); i++){
			
			Employee employee = employees.get(i);
			
			System.out.println("Имя: " + employee.getName()
					+ ", Зарплата: " + employee.getFixedSalary() 
					+ ", Опыт: " + employee.getExperienсe() 
					+ " , Должность: " + employee.getRole());
			for (int j = 0; j < employee.getProjectsCount(); j++){
				Project project =  employee.getProject(j);
				System.out.println(" проект: " + project.getName() 
						+ " " + (project.getFixedCost() ? "fixed":"notFixed"));
			}
		}		
	} 
	
	private void print() {
		Collections.sort(employees);

		for (int i = 0; i < employees.size(); i++){
			System.out.println("Имя: " + employees.get(i).getName() + ", Зарплата: " + employees.get(i).getFullSalary());
		}
		
	}
	
	public static void main (String[] args){
		new SalaryСalculation ();
	}
}
