package salaryСalculation;

public class Project {
	
	private String name;
	private boolean fixedCost;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean getFixedCost() {
		return fixedCost;
	}
	
	public void setFixedCost(boolean fixedCost) {
		this.fixedCost = fixedCost;
	}
	
}
